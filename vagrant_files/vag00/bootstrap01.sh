publicNetworkIfname="Wired connection 1"

command -v "apt-get" > /dev/null 2>&1 && EthernetIfname="enp0s8" # Ubuntu 16
command -v "yum" > /dev/null 2>&1 && EthernetIfname="eth1" # CentOS
cat "/etc/issue" | grep "Debian" && EthernetIfname="eth1" # Debian
command -v "lsb_release" > /dev/null 2>&1 && \
	[ "`lsb_release -i | awk '{ print $3 }'`" == "Ubuntu" ] && \
	lsb_release -r | grep -q '14' && EthernetIfname="eth1" # Ubuntu 14

publicNetworkConName="ens01"
publicNetworkCIDRMask="/24"
publicNetworkNetmask="255.255.255.0"
publicNetworkIP4="192.168.1.121"
publicNetworkGW4="192.168.1.1"
publicNetworkDNS="192.168.1.1"

setNetworkViaNetworkManager() {
	# 如果活动则关闭默认网络连接
	sudo nmcli connection show --active | grep "${publicNetworkIfname}" && \
	sudo nmcli connection modify "${publicNetworkIfname}" autoconnect no && \
	sudo nmcli connection down "${publicNetworkIfname}"
	# 不存在则新建一个网络连接
	sudo nmcli connection show | grep "${publicNetworkConName}" || \
		sudo nmcli connection add con-name ${publicNetworkConName} \
			type ethernet ifname ${EthernetIfname} autoconnect yes \
			ip4 ${publicNetworkIP4}${publicNetworkCIDRMask} \
			gw4 ${publicNetworkGW4} ipv4.dns ${publicNetworkDNS} \
			ipv6.method ignore 
	
	# 正确的路由表
	# default via 192.168.1.1 dev eth1 proto static metric 101
	# default via 10.0.2.2 dev eth0 proto dhcp metric 100
	# 10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 metric 100
	# 192.168.1.0/24 dev eth1 proto kernel scope link src 192.168.1.22 metric 101
}

setNetworkViaIfconfig() {
	ifconfig ${EthernetIfname} ${publicNetworkIP4} \
		netmask ${publicNetworkNetmask} up \
	&& route add default gw ${publicNetworkGW4} \
	&& eval `route -n | awk '{ if ( $8 =="eth0" && $2 !="0.0.0.0" ) \
		print "route del default gw " $2; }'`
}

addPubkey() {
	[ ! -a "~/.ssh" ] && mkdir -p ~/.ssh
	chmod 700 ~/.ssh
	[ ! -a "~/.ssh/authorized_keys" ] && touch ~/.ssh/authorized_keys
	echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA4evk5R/CRYaSXIUqM+SnL3igCuNONfxq\
nEuKJEb2CFpTXLjBpxoo+xSTgiPicVOO3PaTw9HDL/ZbLizdP5RfRwysb3yfaaWKXGlG/2qXeVLZV5\
sfy8l6Qdm67ScvrvAi6knQweZjp3cDpc+yO6RAuRa56R5uj+SHZEk92BpGOpw9juwKy8lnodmCvTrb\
6KsSLG+vGmhiruTcuX4qHQySIogBHYvncE8texwUBZzzM0THtkH697RvLpv/g0IjerRwi/NET0Imrj\
i98WLt/9ZxXrtzrQD1XajOJ6SQKgu2t/FgCwSWfWlYqXEykhhissxYo7YL+qeuUkTRo3VPpgvGtQ==\
 Mail:thanch2n@gmail.com" >> ~/.ssh/authorized_keys && \
	echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCvGmjJxg4A0rrJKhkcDljI2r1z6Xie\
A7FF3Ypn6iXWgmZnfcIzcZrpj4wLWqhdTnp/H09+O75jdcQSpvXR5VmmvmnI2eWb4fPr204SS0Eqoo\
UbF0QgzoxBrorkwY7zq3q4zg1dFsW1unSSI8PpFOhwDK/zk3WBVvdOd8xjPp13bZ0jUV4VhMEOBu5J\
w0PYB0wYwRlXKkTZj+Q1lxKTmyoWT7I5O18QdHqI7wcA1vUl8GINdNR8LPiGcMpaMk71ynHGPE4Pl8\
Cs7EF5Z6Eqw1TPlLikfaJ6/0D87gbitlX4bmysONfYxv3FDm6KlsiUj9zcLSpixV3harsoFP0HWfht\
 Hvming:thanch2n@gmail.com" >> ~/.ssh/authorized_keys && \
	chmod 600 ~/.ssh/authorized_keys
}

command -v "yum" > /dev/null 2>&1 && command -v "nmcli" > /dev/null 2>&1 && \
ip a | grep "${EthernetIfname}" && setNetworkViaNetworkManager # CentOS 7

command -v "yum" > /dev/null 2>&1 && command -v "ifconfig" > /dev/null 2>&1 && \
ip a | grep "${EthernetIfname}" && setNetworkViaIfconfig # CentOS 6

command -v "apt-get" > /dev/null 2>&1 && command -v "ifconfig" > /dev/null 2>&1 && \
ip a | grep "${EthernetIfname}" && setNetworkViaIfconfig # Debian

[ ! -a "~/.ssh/authorized_keys" ] || [ ! -s "~/.ssh/authorized_keys" ] && addPubkey

unset EthernetIfname publicNetworkIP4 publicNetworkNetmask publicNetworkGW4 \
	publicNetworkIfname publicNetworkConName publicNetworkCIDRMask

echo 'Hvming!@#123' | sudo passwd root --stdin
# sudo yum reinstall -y glibc-common
sudo ip route del default
# sudo ip route add default via 10.0.2.2 dev eth0 metric 100