# Esxi_web
---
## 创建虚拟机注意事项
- 调整单颗 CPU 核心数,默认为1
- 硬盘模式可选择 Thin provisioned, 按实际用量计算容量
- 虚拟机中需要使用虚拟化软件如 virtual box ,可勾选 CPU 下暴露硬件虚拟化
- DVD 选择需要安装操作系统的镜像
- 虚拟机中的虚拟机如果需要获取子网 IP, 需要开启交换机安全选项下的 Promiscuous mode