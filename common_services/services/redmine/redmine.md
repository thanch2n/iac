# Redmine
---
## 部署及启动
安装***[[docker]]***后, 使用 docker-compose 启动
```
yum -q -y install git
git clone http://192.168.1.13/chengan/IaC.git
cd IaC/docker-compose/redmine
docker-compose up -d
```
---
## 安装插件
下载解压插件至 ***/usr/src/redmine/plugins/***, 进入该目录运行
```
bundle install --without development test --no-deployment
bundle exec rake redmine:plugins NAME=clipboard_image_paste RAILS_ENV=production
bundle exec rake redmine:plugins NAME=scrum RAILS_ENV=production
```
---
## 卸载插件
```
bundle exec rake redmine:plugins:migrate NAME=clipboard_image_paste VERSION=0 RAILS_ENV=production
bundle exec rake redmine:plugins:migrate NAME=scrum VERSION=0 RAILS_ENV=production
```
## 安装主题
下载解压主题至 ***/usr/src/redmine/public/theme/***