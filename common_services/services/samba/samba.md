# Samba
---
## 访问
- mac: finder 下按下 cmd + K 输入`smb://192.168.1.12/filebox/sector/sector1`
- win: 资源管理器 - 映射网络驱动器, 输入`//192.168.1.12/filebox/sector/sector1`

---
## 个人文件夹
- 地址: `//192.168.1.12/filebox/personal/chengan` ***最后为姓名全拼***