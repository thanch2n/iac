# Vlmcsd
---
## 部署及启动
安装***[[docker]]***后, 使用 docker-compose 启动
```
yum -q -y install git
git clone http://192.168.1.13/chengan/IaC.git
cd IaC/docker-compose/vlmcsd
docker-compose up -d
```

---
## 使用
激活 volume license 版本的 Windows 
```
cd /d "%SystemRoot%\system32"
slmgr /skms 192.168.1.13
slmgr /ato
slmgr /xpr
```
激活 volume license 版本的 Office 
```
cd /d "%ProgramFiles%\Microsoft Office\Office16"
cscript ospp.vbs /sethst:192.168.1.13
cscript ospp.vbs /act
cscript ospp.vbs /dstatus
```